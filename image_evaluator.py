import sys
from pathlib import Path
import os
import subprocess


try:
    import PyQt5
except ImportError:
    subprocess.call(['pip', 'install', 'PyQt5'])
finally:
    from PyQt5 import uic
    from PyQt5.QtWidgets import *
    from PyQt5.QtWidgets import QMainWindow
    from PyQt5 import QtWidgets
    from PyQt5.QtGui import QPixmap
    from PyQt5.QtCore import Qt


script_path = Path(__file__).parent
supported_extensions = [".jpg", ".png"]
files = []
i = 0


class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        uic.loadUi(script_path / 'untitled.ui', self)

        qtRectangle = self.frameGeometry()
        centerPoint = QDesktopWidget().availableGeometry().center()
        qtRectangle.moveCenter(centerPoint)
        self.move(qtRectangle.topLeft())

        self.setFixedSize(self.size())
        self.spinBox.valueChanged.connect(self.spinBoxValueChanged)
        self.spinBox.setMaximum(0)
        self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollArea.setWidgetResizable(True)
        self.openButton.clicked.connect(self.select_folder)
        self.nextButton.clicked.connect(self.nextBtnClick)
        self.prevButton.clicked.connect(self.prevBtnClick)

    def update(self):
        global files
        global i
        n = len(files)
        self.spinBox.setMaximum(n)
        self.IndexLabel.setText(f"{0}/{0}")

        if n > 0:
            self.spinBox.setMaximum(n - 1)
            self.IndexLabel.setText(f"{i + 1}/{n}")
            self.show_img(files[i])
            self.pathLabel.setText(str(files[i]))

    def select_folder(self):
        try:
            global i
            i = 0
            path = Path(str(QFileDialog.getExistingDirectory(self, "Select Directory")))
            update_files(path)
            self.spinBox.setValue(0)
            self.update()
        except Exception as e:
            print(e)

    def show_msg(self, text: str, icon=None):
        msg = QMessageBox()

        if icon is not None:
            msg.setIcon(icon)

        msg.setText(text)
        msg.setWindowTitle("ImageEvaluator")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

    def show_img(self, path):
        if os.path.exists(path):
            pixmap = QPixmap(str(path))
            self.imgLabel.resize(pixmap.size())
            self.imgLabel.setPixmap(pixmap)
            self.imgLabel.setMask(pixmap.mask())

    def nextBtnClick(self):
        self.spinBox.setValue(i + 1)

    def prevBtnClick(self):
        self.spinBox.setValue(i - 1)

    def spinBoxValueChanged(self):
        global i
        i = self.spinBox.value()
        self.update()

    def correctBtnClick(self):
        pass

    def incorrectBtnClick(self):
        pass


def update_files(path: Path):
    p = path.glob('**/*')
    global files
    files = [x for x in p if x.is_file() and x.suffix in supported_extensions and x.stem == "meta"]
    files = sorted(files, key=lambda t: -os.stat(t).st_mtime)


if __name__ == '__main__':
    import warnings

    warnings.filterwarnings("ignore", category=DeprecationWarning)

    app = QtWidgets.QApplication(sys.argv)
    win = MainWindow()
    win.show()
    app.exec_()
